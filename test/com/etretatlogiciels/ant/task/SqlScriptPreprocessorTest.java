package com.etretatlogiciels.ant.task;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import com.etretatlogiciels.ant.task.SqlScriptPreprocessor;

public class SqlScriptPreprocessorTest
{
	private static final String	currentWorkingDirectory  = "/home/russ/blackpearl/rest-server/extras";
	private static final String	inputFilename  = "blackpearl.sql.in";
	private static final String	outputFilename = "blackpearl.sql";
	private static final String HR           = "--------------------------------------------------------------------------------";

	private static SqlScriptPreprocessor	task;

	@Before
	public void init()
	{
		System.out.println();
		System.out.println( HR );

		task = new SqlScriptPreprocessor();
		task.setCurrentworkingdirectory( currentWorkingDirectory );
		task.setInputfilepath( inputFilename );
		task.setOutputfilepath( outputFilename );
	}

	@Test
	public void testSqlPreprocessHelp()
	{
		String[]	args = new String[ 1 ];

		try
		{
			args[ 0 ] = "--help";
			SqlScriptPreprocessor.main( args );
		}
		catch( Exception e )
		{
			fail();
		}
	}

	@Test
	public void testSqlPreprocessVersion()
	{
		String[]	args = new String[ 1 ];

		try
		{
			args[ 0 ] = "--version";
			SqlScriptPreprocessor.main( args );
		}
		catch( Exception e )
		{
			fail();
		}
	}

	@Test
	public void testSqlPreprocess()
	{
		try
		{
			task.execute();
		}
		catch( RuntimeException e )
		{
			fail();
		}
	}
}
