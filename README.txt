This is an SQL script preprocessor for MySQL scripts.

MySQL scripts, as offered to the MySQL command-line tools, can contain "source"
statements that include other scripts. Using these fails, however, with MySQL's
JDBC connector (driver) making it necessary to offer only one long, inconvenient
script.

For greater convenience and versatility, this preprocessor was created as an ant
task or for command-line use.

Apache ant

The details are described at:

- http://ant.apache.org/manual/index.html
- http://ant.apache.org/manual/tutorial-writing-tasks.html

The main steps are:

1. Code the application keeping in mind that it needs to
    a. Extend org.apache.tools.ant.Task.
    b. Fail by throwing org.apache.tools.ant.BuildException.
    c. Create setters for any attributes that will appear in the ant tag.

2. Create a JAR file by running ant in the project root directory. This will execute
    build.xml.

3. Call it inside an ant buildscript thus. The first element defines the utility
    to ant. The second is an imitation target that makes use of the preprocessor.

   <taskdef="sqlscriptpreprocessor"
    classname="com.etretatlogiciels.ant.task.SqlScriptPreprocessor" />

   <target name="mysql-preprocess"
           description="Preprocess MySQL database scripts into one file"
           depends="sqlscriptpreprocessor.jar">
       <sqlscriptpreprocessor inputfilepath="mysql-script.sql.in" outputfilepath="mysql-script.sql" />
   </target>

   Note that there's no "nested text" involved when using this ant task.

Russell Bateman