package com.etretatlogiciels.ant.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

//import org.apache.log4j.Logger;				--only used to debug the logic here/not for production use
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;

/**
 * SQL Script Preprocessor: This is a class that...
 * </p>
 *
 * <ol type="a">
 * <li> implements a preprocessing capability to transform MySQL scripts making use of the
 * <b><tt>source</tt></b> directive into a single, long script that can be consumed by the
 * MySQL JDBC driver (which chokes on the "source" directive).
 * </li>
 * <dd />
 * <li> implements this as an Apache ant task for consumption from an ant buildscript. </li>
 * </ol>
 *
 * <p>
 * This preprocessor features...
 * </p>
 *
 * <ul>
 * <li> the ability to specify a current working directory relative to which all paths,
 * whether those specified here or in the SQL script(s), are calculated. Full paths are
 * also accepted and are not reinterpreted.
 * </li>
 * <dd />
 * <li> the ability to suppress the output of status messages. </li>
 * </ul>
 *
 * @author Russell Bateman
 * @since October 2011
 */
public class SqlScriptPreprocessor extends Task
{
//	private static final Logger	log	    = Logger.getLogger( SqlScriptPreprocessor.class );
	private static final String	version = "1.00.1 (28 October 2011)";
	private boolean				quiet;
	private FileWriter			out;
	private String				currentWorkingDirectory;
	private String				inputFilePath;
	private String				outputFilePath;

	public SqlScriptPreprocessor()
	{
		this.out                     = null;
		this.quiet                   = false;
		this.currentWorkingDirectory = "";
		this.inputFilePath           = "";
		this.outputFilePath          = "";
	}

	/* ------------ This is the Apache task section --------------------------------
	 *
	 * These setters are for an Apache ant task and have, therefore, rather peculiar
	 * casing problems which we tolerate. After calling the mandatory ones, any
	 * consumer simply calls execute.
	 */
	public final void setQuietmode( boolean quietMode )          { this.quiet = quietMode; }
	public final void setCurrentworkingdirectory( String cwd )   { this.currentWorkingDirectory = cwd; }
	public final void setInputfilepath( String inputFilename )   { this.inputFilePath = inputFilename; }
	public final void setOutputfilepath( String outputFilename ) { this.outputFilePath = outputFilename; }
	/* ----------------------------------------------------------------------------- */

	/**
	 * The (sole) output stream is initialized and the methods doing the heavy lifting
	 * underneath are called.
	 */
	public final void execute() throws BuildException
	{
		/* First, we find out the name of the ant project, if there is one, and
		 * write that to the log. This is stuff that is only available when running
		 * under ant. To be sure of what's passed in ant, use the actual, recorded
		 * properties and not what was stuffed in using the setters.
		 */
		Project	antProject = getProject();

		if( antProject != null )
		{
			String	logMessage = antProject.getProperty( "ant.project.name" );

			if( logMessage != null && logMessage.length() > 0 )
			{
				log( "Preprocessing " + logMessage + " from " + getLocation() );

				String	inFilename  = getProject().getProperty( "inputfilepath" );
				String	outFilename = getProject().getProperty( "outputfilepath" );

				log( "    " + outFilename + " <-- " + inFilename );
			}
		}

//		log.trace( "execute()--open output stream wrapper" );

		if(    this.inputFilePath == null
			|| this.inputFilePath.length() < 1
			|| this.outputFilePath == null
			|| this.outputFilePath.length() < 1 )
			throw new BuildException( "SQL script preprocessing indequately inititialized." );

		File	fOut;
		String	message = "";
		String	fullOutputPath = renderFullPath( this.outputFilePath );

//		log.trace( "execute( String, String )--open output stream wrapper" );

		try
		{
			/* Open the output file. If it already exists, erase it before appending to it.
			 */
			fOut = new File( fullOutputPath );

			if( fOut.exists() )
				fOut.delete();

			this.out = new FileWriter( fOut, true );
		}
		catch( IOException e )
		{
			message = "Error opening" + this.outputFilePath;
			System.out.println( message );
			throw new BuildException( "SQL script preprocessing: " + message );
		}

		try
		{
			if( !processSqlScript( this.inputFilePath ) )
				throw new BuildException( "SQL script preprocessing failed." );
		}
		catch( NullPointerException e )
		{
			System.out.println( "Error  null pointer exception" );
			throw new BuildException( "SQL script preprocessing: ." );
		}
		finally
		{
			/* Close the output file.
			 */
			try
			{
				if( this.out != null )
					this.out.close();
			}
			catch( IOException e )
			{
				message = "Error closing" + fullOutputPath;
				System.out.println( message );
				throw new BuildException( "SQL script preprocessing: " + message );
			}
		}
	}

	/**
	 * Called on a new file (known by reason of an include marker appearing in the script
	 * text) that must be copied onto the existing stream. The input stream to read from
	 * is opened here.
	 * </p>
	 *
	 * <p>
	 * This is potentially called recursively&mdash;in the case where this script includes
	 * another. It's really just a wrapper around the method that does the heavy lifting.
	 * </p>
	 *
	 * @param newFilename the new file to open and read from.
	 * @return true or false that we're blissfully happy about what's gone on.
	 */
	public boolean processSqlScript( String newFilename )
	{
		FileReader		rIn   = null;
		BufferedReader	in    = null;
		String			error = "unknown";
		String			fullInputPath = renderFullPath( newFilename );

//		log.trace( "processSqlScript( String )--open file wrapper" );

		try
		{
			/* Open the new input file. Note: use File to see check its existence, and a
			 * FileReader so we can open it on a BufferedReader
			 */
			error = "opening input file" + fullInputPath;

			File	fIn = new File( fullInputPath );

			if( !fIn.exists() )
				throw new FileNotFoundException();

			rIn = new FileReader( fIn );
			in  = new BufferedReader( rIn );

			boolean result = processScriptLines( in );

			if( !result )
				throw new RuntimeException( "SQL preprocessing failed" );
		}
		catch( NullPointerException e )
		{
			System.out.println( "Error " + error );
			return false;
		}
		catch( FileNotFoundException e )
		{
			System.out.println( "Error " + error + ": file does not exist" );
			return false;
		}
		finally
		{
			/* Close the input stream as we've finished with it.
			 */
			try
			{
				if( in != null )
					in.close();
			}
			catch( IOException e )
			{
				System.out.println( "Error closing" + fullInputPath );
				return false;
			}
		}

		return true;
	}

	private static final String	INCLUDE_MARKER = "source ";
	private static final char	INCLUDE_END    = '\0';

	/**
	 * The heavy lifting is done here by reading from the input stream and copying the
	 * contents onto the output stream pausing only when it finds an include marker.
	 * </p>
	 *
	 * <p>
	 * If an include marker is found, isolate the name of the subordinate file to be
	 * included, then, suspending the present input stream, insert the contents of the
	 * new file by calling our caller (recursively).
	 * </p>
	 *
	 * @param in the (buffered) input stream from which to read characters.
	 * @return true or false that we're blissfully happy about what's gone on.
	 */
	public boolean processScriptLines( BufferedReader in )
	{
		String	error      = "unknown";
		char	includeEnd = INCLUDE_END;

//		log.trace( "processScriptLines( BufferedReader )--heavy lifting" );

		try
		{
			String	line;

			/* Read through the input file copying its contents to the output except and unless
			 * preprocessing markers are encountered at which time, copying halts and the marker,
			 * usually an include statement, is processed and copying begins again.
			 */
			error = "reading file";

			while( ( line = in.readLine() ) != null )
			{
				/* Look for markers...
				 */
				int	pos, end;

				if( ( pos = line.indexOf( INCLUDE_MARKER ) ) != -1 )
				{
					/* The include marker means, "Here's a file to include at this very
					 * point before continuing on."
					 */
					pos += INCLUDE_MARKER.length();
					end  = line.indexOf( INCLUDE_END, pos );

					/* Here is the name of the filepath to open and whose contents are
					 * to be inserted right here. We call ourselves recursively to make
					 * that happen.
					 */
					String newFilename;

					if( includeEnd != 0 )
					{
						/* Normally, as in MySQL, the construct is "source <path>" with no
						 * terminator. We coded this up just in case we ever wanted to redo it
						 * using a more deliberate construct such as "<include path>".
						 */
						if( end == -1 )
							throw new RuntimeException( "Unterminated include marker" );

						newFilename = line.substring( pos, end );
					}
					else
					{
						newFilename = line.substring( pos );
					}

					if( !this.quiet )
						System.out.println( "    Including " + newFilename + "..." );
					this.out.write( "-- <<< Script content from " + newFilename + ": >>>\n" );

					// recursive call to our caller...
					processSqlScript( newFilename );

					line = ""; // (since we do not wish to copy out the include command)
				}

				/* Write out what we've got so far...
				 */
				error = "writing to output";
				this.out.write( line + "\n" );
				error = "reading file";
			}
		}
		catch( IOException e )
		{
			System.out.println( "Error " + error );
			return false;
		}

		return true;
	}

	private static final String	BACKUP  = "../";
	private static final String	THISDIR = "./";

	/**
	 * Count the number of parents to back up to in the current working directory.
	 *
	 * @param path current file path.
	 * @return number of occurrences of "../" at beginning of <tt>path</tt>.
	 */
	private static final int countDirectoryDepth( String path )
	{
		int	depth = 0;
		int	pos   = 0;

		while( path.startsWith( "../", pos ) )
		{
			depth++;
			pos += BACKUP.length();
		}

		return depth;
	}

	/**
	 * Reverse the passed-in string for whatever convenience this might present to the
	 * caller.
	 *
	 * @param string to be reversed.
	 * @return the reversed string.
	 */
	private static final String strrev( String string )
	{
	    int				i, length = string.length();
	    StringBuffer	target = new StringBuffer( length );

	    for( i = length - 1; i >= 0; i-- )
	      target.append( string.charAt( i ) );

	    return target.toString();
	}

	/**
	 * Snip the indicated number of subdirectories from the copy of the current working
	 * directory passed in.
	 *
	 * @param cwd copy of the current working directory.
	 * @return the resulting, truncated current working directory.
	 */
	private static final String truncateCwd( String cwd, int count )
	{
		String	reverse  = strrev( cwd );

		while( count > 0 )
		{
			int	snipTo = reverse.indexOf( '/' );

			if( snipTo == -1 )
				break;

			reverse = reverse.substring( snipTo+1 );
		}

		return strrev( reverse );
	}

	/**
	 * From the "current working directory," render a full (absolute) path name. This is
	 * essential since Java file I/O knows no concept of "current working directory." We
	 * have to do it ourselves.
	 *
	 * @param path relative or absolute path to be transformed.
	 * @return an absolute path on the host executing this code.
	 */
	private final String renderFullPath( String path )
	{
		String	renderedPath;

		switch( path.charAt( 0 ) )
		{
			case '/' :				// don't massage paths that already start at the root...
				renderedPath = path;
				break;

			case '.' :
				String	newCwd;
				String	newPath;

				/* Either the path begins with "./" or with one or more "../". Simply remove
				 * the first or count the number of the second and remove them from the front
				 * of the path. In the latter case, also truncate a copy of the current
				 * working directory by the same number of paths. Form the final path by
				 * prepending (what's left of) the current working directory path to what was
				 * passed in.
				 */
				int	depth = ( path.startsWith( BACKUP ) ) ? countDirectoryDepth( path ) : 0;

				if( depth == 0 )
				{
					newCwd  = this.currentWorkingDirectory;
					newPath = path.substring( THISDIR.length() );
				}
				else
				{
					newCwd  = truncateCwd( currentWorkingDirectory, depth );
					newPath = path.substring( BACKUP.length() * depth );
				}

				renderedPath = newCwd + "/" + newPath;
				break;

			default :
				/* This is merely a filepath or filename with no (relative or absolute)
				 * subdirectory indication at its beginning.
				 */
				renderedPath = this.currentWorkingDirectory + "/" + path;
				break;
		}

		return renderedPath;
	}

	private static void doHelp()
	{
		System.out.println( "SqlPreprocess - preprocesses an SQL script including another script." );
		System.out.println();
		System.out.println( "Possible invocations:" );
		System.out.println( "    java SqlPreprocess --help" );
		System.out.println( "    java SqlPreprocess --version" );
		System.out.println( "    java SqlPreprocess [-quiet][-cwd=path] <input-scriptname> <output-script>" );
		System.out.println();
		System.out.println( "(Option -quiet optionally suppresses console output of the list of" );
		System.out.println( "    included filepaths during processing.)" );
	}

	private static void doVersion()
	{
		System.out.println( "SqlPreprocess, " + version );
		System.out.println();
	}

	/**
	 * This is how this class is used as a command-line utility. It's invoked thus:
	 * </p>
	 *
	 * <pre>
	 * java SqlPreprocess [-quiet] dbscript.sql.in dbscript.sql
	 * </pre>
	 *
	 * <p>
	 * ...and simply preprocesses the script input file (which may contain include
	 * markers) into the output (final, runnable) script to be used in the normal
	 * build process.
	 * </p>
	 *
	 * <p>
	 * We've taken care to launch this just as if we were ant.
	 * </p>
	 *
	 * @param args command-line argument list
	 */
	public static void main( String[] args )
	{
		int		argCount = args.length;
		String	inputFilename = null, outputFilename = null;

		SqlScriptPreprocessor	task = new SqlScriptPreprocessor();

		for( int first = 0; argCount > 0; first++, argCount-- )
		{
			String	argument = args[ first ];

			if( argument.equals( "--help" ) )
			{
				doHelp();
				return;
			}

			if( argument.equals( "--version" ) )
			{
				doVersion();
				return;
			}

			if( argument.equals( "-quiet" ) )
			{
				task.setQuietmode( true );
				continue;
			}

			if( argument.startsWith( "-cwd=" ) )
			{
				String	cwd = argument.substring( "-cwd=".length() );

				if( cwd.length() > 0 )
					task.setCurrentworkingdirectory( cwd );

				continue;
			}

			if( inputFilename == null )
			{
				task.setInputfilepath( inputFilename = argument );
				continue;
			}

			if( outputFilename == null )
			{
				task.setOutputfilepath( outputFilename = argument );
				continue;
			}
		}

		try
		{
			task.execute();
		}
		catch( RuntimeException e )
		{
			System.out.println( "SQL preprocessing failed; " + e.getMessage() );
		}
	}
}
